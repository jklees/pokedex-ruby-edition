FROM ruby:slim-stretch

RUN apt-get update && \
    apt-get install -y build-essential

ENV APP_HOME /pokedex
WORKDIR $APP_HOME

RUN gem install bundler

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

EXPOSE 9393