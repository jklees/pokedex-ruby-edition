require 'sinatra/base'
require 'net/http'
require 'json'
require './lib/pokemon'

module Pokedex
  class InternalAPI < Sinatra::Base
    helpers JSON

    # within these routes we could make calls to a service/library
    # which will do the work of calling the Pokemon API,
    # retrieving from and storing data in the cache, and raising errors.

    # store new Pokemon in the cache
    post '/:name' do
      Libs::Pokemon.save(params['name'])
    end

    # TODO: return the data from the cache so that the front-end can use it
    get '/cache_list' do
      content_type :json
      pokemon = Libs::Pokemon.get_all()
    end

    # TODO: get pokemon list from the Pokemon API to create the front-end menu
    get '/list' do
      # move logic into library
      limit = params['limit']
      offset = params['offset']
      uri = URI("https://pokeapi.co/api/v2/pokemon?limit=#{limit}&offset=#{offset}")
      resp = JSON.parse(Net::HTTP.get(uri))

      JSON(data: resp)
    end
  end
end
