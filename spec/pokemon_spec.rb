require './lib/pokemon'
require 'spec_helper'

describe Pokedex::Libs::Pokemon do
  let!(:valid_name) { 'ditto' }
  let!(:invalid_name) { 'pokenone' }

  describe '.save' do
    it 'returns json string when provided with valid Pokemon name' do
      hash = { id: 1 }
      allow(described_class).to receive(:find_pokemon_by_name).and_return(hash)
      allow(described_class).to receive(:save_to_database).and_return(true)

      expected_json = '{"id":1}'
      expect(described_class.save(valid_name)).to eq(expected_json)
    end

    it 'raises 404 error when provided with invalid Pokemon name' do
      allow(described_class).to receive(:find_pokemon_by_name).and_return(nil)
      expect { described_class.save('pokenone') }.to raise_error { |error|
        expect(error).to be_a(Pokedex::Libs::ErrorWithStatus)
        expect(error.status_code).to eq(404)
      }
    end

    it 'raises 500 error when DB exception rescued' do
      hash = { unknown_attr: 'not valid' }
      allow(described_class).to receive(:find_pokemon_by_name).and_return(hash)
      allow(described_class).to receive(:save_to_database).and_raise(Mongoid::Errors::UnknownAttribute)
      expect { described_class.save('pokenone') }.to raise_error { |error|
        expect(error).to be_a(Pokedex::Libs::ErrorWithStatus)
        expect(error.status_code).to eq(500)
      }
    end
  end

  describe 'external calls to Pokemon API' do
    describe '.find_pokemon_by_name' do
      let!(:url) { 'https://pokeapi.co/api/v2/pokemon/' }
      let!(:req_headers) do
        { 'Accept' => '*/*',
          'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'Host' => 'pokeapi.co',
          'User-Agent' => 'Ruby' }
      end

      it 'returns hash when valid name parameter is given' do
        resp_body = '{"id":1}'
        stub_request(:get, url + valid_name)
          .with(headers: req_headers).to_return(status: 200, body: resp_body, headers: {})

        expected_hash = { id: 1 }
        expect(described_class.send(:find_pokemon_by_name, valid_name)).to include(expected_hash)
      end

      it 'returns nil when invalid name parameter is given' do
        stub_request(:get, url + invalid_name)
          .with(headers: req_headers).to_return(status: 404, body: 'null', headers: {})

        expect(described_class.send(:find_pokemon_by_name, invalid_name)).to be_nil
      end
    end
  end
end
