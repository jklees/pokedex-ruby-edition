require 'sinatra/base'
require 'json'
require 'net/http'
require './models/pokemon'

module Pokedex
  module Libs
    class ErrorWithStatus < StandardError
      attr_reader :status_code
      def initialize(status_code, message)
        @status_code = status_code
        super(message)
      end
    end

    class Pokemon < Sinatra::Base
      helpers JSON

      # Not doing anything interesting with this yet
      # just want to check pokemon are being saved
      def self.get_all()
        Models::Pokemon.all.to_json
      end

      def self.save(name)
        pokemon = find_pokemon_by_name(name)

        raise ErrorWithStatus.new(404, 'Pokemon not found') if pokemon.nil?

        # TODO: How to deal with Mongoid/MongoDB errors properly
        begin
          save_to_database(pokemon)
        rescue 
          raise ErrorWithStatus.new(500, 'Pokemon could not be saved')
        end 

        pokemon.to_json
      end

      class << self
        private

        def find_pokemon_by_name(name)
          uri = URI("https://pokeapi.co/api/v2/pokemon/#{name}")
          data = JSON.parse(Net::HTTP.get(uri))

          unless data.nil?
            data = {
              id: data['id'],
              height: data['height'],
              weight: data['weight'],
              species: data['species']
            }
          end

          data
        end

        def save_to_database(data)
          cache_pokemon = Models::Pokemon.new(data)
          cache_pokemon.save!
        end
      end
    end
  end
end
