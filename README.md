## Run without Docker

Install Bundler, see: [https://bundler.io](https://bundler.io)

    # install dependencies specified in the Gemfile
    bundle install

    # start the application
    bundle exec shotgun config.ru

Go to: [http://localhost:9393](http://localhost:9393)

## Run with Docker
    # to start sinatra and mongo containers
    docker-compose up -d sinatra 

Go to: [http://localhost:9393](http://localhost:9393)

### Working with app containers
    # run bash inside a second sinatra container 
    # interactive ruby or rspec commands (for example) can be 
    # executed from here
    docker-compose run --rm sinatra bash

    # run bash inside existing mongo container
    docker exec -it <running-container-name> bash

## Tools and info

### Run the Rubocop linter
    #list offences
    bundle exec rubocop 

    # correct offenses
    bundle exec rubocop -a

### Run Rspec tests
    #all
    bundle exec rspec
    #run tests in a specified file
    bundle exec rspec [relative_path_to_file]
    #run a specific test (or tests)
    bundle exec rspec [relative_path_to_file]:[line_no]
    
    #example
    bundle exec rspec spec/pokemon_spec.rb:16

### Test Internal API post request
    curl -d "name=ditto" -X POST http://localhost:9393/internal_api/ditto

### To see pokemon MongoDB collection
    To see the populated collection, make internal API post requests as shown above: 

    http://localhost:9393/internal_api/cache_list





    
