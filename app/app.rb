require 'sinatra/base'

module Pokedex
  class ApplicationController < Sinatra::Base
    configure do
      set :views, 'app/views'
    end

    get '/' do
      erb :panel
    end
  end
end
