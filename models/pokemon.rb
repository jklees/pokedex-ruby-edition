require 'mongoid'

module Pokedex 
  module Models
    class Pokemon
        include Mongoid::Document
        store_in collection: "pokemon"
        field :id, type: Integer
        field :height, type: Integer
        field :weight, type: Float
        field :species
    end
  end
end