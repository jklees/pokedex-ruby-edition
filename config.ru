require 'sinatra'
require 'mongoid'
require './app/app'
require './internal_api/app'

configure do
  Mongoid.load! "mongoid.config"

  map '/' do
    run Pokedex::ApplicationController
  end
    
  map '/internal_api' do
    run Pokedex::InternalAPI
  end
end


